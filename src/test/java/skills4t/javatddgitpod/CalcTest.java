package com.skills4t.javatddgitpod;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalcTest {

    @Test
    public void getAddOneOnOne() {
        Calc calc = new Calc();
        assertEquals(2, calc.add(1,1));
    }

    @Test
    public void getAdd3on4is7() {
        Calc calc=new Calc();
        assertEquals(7, calc.add(3,4));
    }
}
